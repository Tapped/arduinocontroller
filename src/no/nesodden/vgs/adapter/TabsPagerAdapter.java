/*	
 * Copyright (C) 2014 Emil Sandst�
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: emilalexer@hotmail.com
 */

package no.nesodden.vgs.adapter;

import no.nesodden.vgs.arduinocontroller.AnalogIn;
import no.nesodden.vgs.arduinocontroller.DigitalInFragment;
import no.nesodden.vgs.arduinocontroller.PWMFragment;
import no.nesodden.vgs.arduinocontroller.DigitalOutFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class TabsPagerAdapter extends FragmentStatePagerAdapter {

	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int arg0) {
		switch(arg0)
		{
		case 0:
			return new DigitalOutFragment();
		case 1:
			return new PWMFragment();
		case 2:
			return new DigitalInFragment();
		case 3:
			return new AnalogIn();
		}
		return null;
	}

	@Override
	public int getCount() {
		return 4;
	}
}
