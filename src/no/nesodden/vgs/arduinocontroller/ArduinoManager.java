package no.nesodden.vgs.arduinocontroller;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Map;

import no.nesodden.vgs.arduinocontroller.AnalogIn.RequestHandler;

public class ArduinoManager {
	Map<Integer, Boolean> digitalOutStates = new HashMap<Integer, Boolean>();
	MainActivity mHandler;
	AnalogIn mAnalogIn;
	
	private static final int DIGITAL_OUTPUT = 0;
	private static final int ANALOG_OUTPUT = 1;
	private static final int REQ_ANALOG_INPUT = 2;
	private static final int ANALOG_INPUT_RES = 3;
	
	public ArduinoManager(MainActivity handler) {
		mHandler = handler;
	}
	
	public void onReceive(byte[] recvBuf, int size) {
		if(recvBuf[0] == ANALOG_INPUT_RES) {
			// We received a result from one of our requests.
			if(mAnalogIn != null) {
				// Convert the bytes to a short.
				ByteBuffer bb = ByteBuffer.allocate(2);
				bb.order(ByteOrder.LITTLE_ENDIAN);
				bb.put(recvBuf[2]);
				bb.put(recvBuf[3]);
				
				mAnalogIn.setAnalogPin((int)recvBuf[1], bb.getShort(0));
			}
		}
	}
	
	public void setDigitalOut(int pin, Boolean state) {
		Boolean currentVal = digitalOutStates.get(pin);
		digitalOutStates.put(pin, state);
		
		if(mHandler.mConnectedThread != null) {
			mHandler.mConnectedThread.write(new byte[] {
				(byte)DIGITAL_OUTPUT, (byte)pin, (byte)(digitalOutStates.get(pin) ? 1 : 0)
			});
		}
	}
	
	public void reguestAnalogIn(int pin, AnalogIn handler) {
		if(mHandler.mConnectedThread != null) {
			mAnalogIn = handler;
			mHandler.mConnectedThread.write(new byte[] {
				(byte)REQ_ANALOG_INPUT, (byte)pin, 0
			});
		}
	}
	
	public void setAnalogOut(int pin, int value) {
		if(mHandler.mConnectedThread != null){
			mHandler.mConnectedThread.write(new byte[] {
					(byte)ANALOG_OUTPUT, (byte)pin, (byte)value
				});
		}
	}
}