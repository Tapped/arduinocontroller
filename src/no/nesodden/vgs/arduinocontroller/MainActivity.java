/*	
 * Copyright (C) 2014 Emil Sandst�
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: emilalexer@hotmail.com
 */

package no.nesodden.vgs.arduinocontroller;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.UUID;

import no.nesodden.vgs.adapter.TabsPagerAdapter;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class MainActivity extends SherlockFragmentActivity implements ActionBar.TabListener {
	private static final int REQUEST_ENABLE_BT = 1;
	private ViewPager mViewPager;
	private TabsPagerAdapter mAdapter;
	private ActionBar mActionBar;
	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothDevice mDevice = null;
	
	//private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	//private static final UUID MY_UUID = UUID.fromString("04c6093b-0000-1000-8000-00805f9b34fb");
	private static final String TAG = "ArduinoController";
	private String [] tabTitles = {"Digital Out", "Analog out", "Digital In", "Analog In"};
	
	public static ConnectedThread mConnectedThread;
	public static ArduinoManager arduinoMgr;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		arduinoMgr = new ArduinoManager(this);
		
        mViewPager = (ViewPager)findViewById(R.id.pager);
        mActionBar = getSupportActionBar();
        mAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        
        mViewPager.setAdapter(mAdapter);
        mActionBar.setHomeButtonEnabled(false);
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        for (String tabTitle : tabTitles) {
        	mActionBar.addTab(mActionBar.newTab().setText(tabTitle).setTabListener(this));
        }
        
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                mActionBar.setSelectedNavigationItem(position);
            }
 
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }
 
            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
        
		// Setup bluetooth.
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if(mBluetoothAdapter == null) {
			Log.d("BTSerial", "Device does not support bluetooth.");
		} else {
			if(!mBluetoothAdapter.isEnabled()) {
				Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
			} else {
				initBT();
			}
		}	
	}
	
	private void initBT() {
		Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
		if (pairedDevices.size() > 0) {
		    for (BluetoothDevice device : pairedDevices) {
		    	// Check if we have paired to "linvor" before.
		    	if(device.getName().equals("linvor")) {
		    		mDevice = device;
		    	}
		    }
		}
		
		// We have never paired to "linvor" before, so lets try to discover it.
		if(mDevice == null) {
			// Show a popup for the user.
			AlertDialog.Builder builder = new AlertDialog.Builder(this);

			builder.setMessage("The bluetooth module linvor is not paired. " +
								"Do that from your bluetooth settings.")
			       .setTitle("Couldn't find bluetooth module.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                       public void onClick(DialogInterface dialog, int id) {
                           finish();
                       }
                   });
			
			AlertDialog dialog = builder.create();
			dialog.show();
		}
		else {
			new ConnectThread().start();
		}
	}
	
	private BluetoothSocket createBTSocket() {
		try {
			final Method  m = mDevice.getClass().getMethod("createRfcommSocket", new Class[] { int.class });
			return (BluetoothSocket) m.invoke(mDevice, 1);
		} catch (Exception e) {
			Log.e(TAG, "Could not create Insecure RFComm Connection", e);
		}
		
		return null;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == REQUEST_ENABLE_BT) {
			if(resultCode == RESULT_OK) {
				initBT();
			}
		}
	}
	
	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}
	
	private class ConnectThread extends Thread {
	    private final BluetoothSocket mmSocket;
	 
	    public ConnectThread() {
	        mmSocket = createBTSocket();
	    }
	 
	    public void run() {
	        // Cancel discovery because it will slow down the connection
	        mBluetoothAdapter.cancelDiscovery();
	 
	        try {
	            // Connect the device through the socket. This will block
	            // until it succeeds or throws an exception
	            mmSocket.connect();
	            
	            // Start our thread for communication.
		        mConnectedThread = new ConnectedThread(mmSocket);
		        mConnectedThread.start();  
	        } catch (IOException connectException) {
	            // Unable to connect; close the socket and get out
	            try {
	                mmSocket.close();
	            } catch (IOException closeException) { }
	            return;
	        }
	    }
	 
	    /** Will cancel an in-progress connection, and close the socket */
	    public void cancel() {
	        try {
	            mmSocket.close();
	        } catch (IOException e) { }
	    }
	}
}
