/*	
 * Copyright (C) 2014 Emil Sandst�
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: emilalexer@hotmail.com
 */

package no.nesodden.vgs.arduinocontroller;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ToggleButton;

public class DigitalOutFragment extends Fragment implements View.OnClickListener {
	private View rootView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_digitalout, container, false);
		
		setButtonToListener(R.id.button1);
		setButtonToListener(R.id.button2);
		setButtonToListener(R.id.button3);
		setButtonToListener(R.id.button4);
		setButtonToListener(R.id.button5);
		setButtonToListener(R.id.button6);
		setButtonToListener(R.id.button7);
		setButtonToListener(R.id.button8);
		setButtonToListener(R.id.button9);
		setButtonToListener(R.id.button10);
		setButtonToListener(R.id.button11);
		setButtonToListener(R.id.button12);
		
		return rootView;
	}

	@Override
	public void onClick(View v) {
		ToggleButton b = (ToggleButton)v;
		MainActivity.arduinoMgr.setDigitalOut(getIntFromPIN(b.getText().toString()), b.isChecked());
	}
	
	private int getIntFromPIN(String pin) {
		for(int i = 0;i < pin.length();++i) {
			// Check if current char is in the ASCII code range for digits.
			if(pin.charAt(i) >= 48 && pin.charAt(i) < 58) {
				return Integer.parseInt(pin.substring(i));
			}
		}
		
		return -1;
	}
	
	private void setButtonToListener(int id) {
		ToggleButton but = (ToggleButton)rootView.findViewById(id);
		but.setOnClickListener(this);
	}
}
