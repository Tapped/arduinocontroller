/*	
 * Copyright (C) 2014 Emil Sandst�
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: emilalexer@hotmail.com
 */
package no.nesodden.vgs.arduinocontroller;

import java.util.ArrayList;
import java.util.List;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

public class AnalogIn  extends Fragment implements View.OnClickListener {
	private static final long delay = 500;
	private View rootView;
	TextView[] analogTextViews = new TextView[6]; 
	
	RequestHandler mRequestHandler;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_analogin, container, false);
		
		mRequestHandler = new RequestHandler(this);
		
		setButtonToListener(R.id.AButton1);
		setButtonToListener(R.id.AButton2);
		setButtonToListener(R.id.AButton3);
		setButtonToListener(R.id.AButton4);
		setButtonToListener(R.id.AButton5);
		
		analogTextViews[0] = (TextView) rootView.findViewById(R.id.out1);
		analogTextViews[1] = (TextView) rootView.findViewById(R.id.out2);
		analogTextViews[2] = (TextView) rootView.findViewById(R.id.out3);
		analogTextViews[3] = (TextView) rootView.findViewById(R.id.out4);
		analogTextViews[4] = (TextView) rootView.findViewById(R.id.out5);
		
		mRequestHandler.sendEmptyMessage(0);
		
		return rootView;
	}
	
	@Override
	public void onClick(View v) {
	}
	
	public void setAnalogPin(int pin, short value) {
		if(MainActivity.arduinoMgr.mHandler != null) {
			try
			{
				MainActivity.arduinoMgr.mHandler.runOnUiThread(new UpdateAnalogOut(analogTextViews[pin], value));
			}catch(Exception e) {
			}
		}
	}
	
	public static int getIntFromPIN(String pin) {
		for(int i = 0;i < pin.length();++i) {
			// Check if current char is in the ASCII code range for digits.
			if(pin.charAt(i) >= 48 && pin.charAt(i) < 58) {
				return Integer.parseInt(pin.substring(i));
			}
		}
		
		return -1;
	}
	
	private void setButtonToListener(int id) {
		ToggleButton but = (ToggleButton)rootView.findViewById(id);
		but.setOnClickListener(this);
		mRequestHandler.addAnalogListener(but);
	}
	
	private static class UpdateAnalogOut implements Runnable {
		private final TextView view;
	    private final short value;
	
	    UpdateAnalogOut(TextView view, short value) {
	    	this.view = view; 
			this.value = value;
		}
	
	     public void run() {
	    	 view.setText(Short.toString(value));
	     }
	 }
	
	// Handles analog input requests
	public static class RequestHandler extends Handler {
		List<ToggleButton> mAnalogIns = new ArrayList<ToggleButton>();
		AnalogIn mParent;
		public RequestHandler(AnalogIn parent) {
			mParent = parent;
		}
		
		@Override
		public void handleMessage(Message msg) {
			sendEmptyMessageDelayed(0, delay);
			for(ToggleButton b : mAnalogIns) {
				if(b.isChecked()) {
					MainActivity.arduinoMgr.reguestAnalogIn(getIntFromPIN(b.getText().toString()), mParent);
				}
			}
        }
		
		public void addAnalogListener(ToggleButton b) {
			mAnalogIns.add(b);
		}
		
		public void removeAnalogListener(ToggleButton b) {
			mAnalogIns.remove(b);
		}
	};
}
