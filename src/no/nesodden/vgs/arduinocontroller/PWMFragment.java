/*	
 * Copyright (C) 2014 Emil Sandst�
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: emilalexer@hotmail.com
 */

package no.nesodden.vgs.arduinocontroller;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class PWMFragment extends Fragment implements OnSeekBarChangeListener {
	View mRootView;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.fragment_pwm, container, false);
		
		setSeekBarListener(R.id.seekBar1);
		setSeekBarListener(R.id.seekBar2);
		setSeekBarListener(R.id.seekBar3);
		setSeekBarListener(R.id.seekBar4);
		setSeekBarListener(R.id.seekBar5);
		setSeekBarListener(R.id.seekBar6);
		
		return mRootView;
	}
	
	private void setSeekBarListener(int id) {
		SeekBar seekBar = (SeekBar)mRootView.findViewById(id);
		seekBar.setOnSeekBarChangeListener(this);
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		MainActivity.arduinoMgr.setAnalogOut(Integer.parseInt((String)seekBar.getTag()), 
											(int)(2.55 * (float)progress));
	}
	
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
	}
}
