/*	
 * Copyright (C) 2014 Emil Sandstø
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contact: emilalexer@hotmail.com
 */
char msg[3];

enum MessageTypes{DIGITAL_OUT = 0, ANALOG_OUT = 1, REQ_ANALOG_INPUT = 2, ANALOG_INPUT_RES = 3};

int tmpAnalogIn = 0;

// the setup routine runs once when you press reset:
void setup() {                
  Serial.begin(9600); 
}

// the loop routine runs over and over again forever:
void loop() {
  if (Serial.available() >= 3) {
    Serial.readBytes(msg, 3);
    
    switch(msg[0])
    {
      case DIGITAL_OUT:
        pinMode(msg[1], OUTPUT);  
        digitalWrite(msg[1], msg[2]);
        break;
      case ANALOG_OUT:
        pinMode(msg[1], OUTPUT);
        analogWrite(msg[1], msg[2]);
        break;
      case REQ_ANALOG_INPUT:
        tmpAnalogIn = analogRead(msg[1]);
        Serial.write((char)ANALOG_INPUT_RES);
        Serial.write(msg[1]);
        Serial.write(tmpAnalogIn & 255); // I decompose the 16 bit number to two bytes. Little endian format.
        Serial.write(tmpAnalogIn >> 8);
        break;
    }
  }
}
